import java.util.ArrayList;
import java.util.List;

public class CodeforcesSubmission {
    private final int submissionId;
    private final int contestId;
    private final String problemName;
    private final String programmingLanguage;
    private final String handle;
    private final String verdict;
    private final String jsonString;
    private String descriptions;

    private final List<CodeforcesTestCase> testCases = new ArrayList<>();
    private final List<CodeforcesTestCase> exampleTests = new ArrayList<>();

    private String sourceCode;

    public CodeforcesSubmission(int submissionId, int contestId, String problemName, String programmingLanguage, String handle, String verdict, String jsonString) {
        this.submissionId = submissionId;
        this.contestId = contestId;
        this.problemName = problemName;
        this.programmingLanguage = programmingLanguage;
        this.handle = handle;
        this.verdict = verdict;
        this.jsonString = jsonString;
    }

    public List<CodeforcesTestCase> getTestCases() {
        return testCases;
    }

    public List<CodeforcesTestCase> getExampleTests() {
        return exampleTests;
    }


    public void addExampleTestCase(CodeforcesTestCase test) {
        exampleTests.add(test);
    }


    public void addTestCase(CodeforcesTestCase test) {
        testCases.add(test);
    }

    public String getSourceCode() {
        return sourceCode;
    }

    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }

    public int getSubmissionId() {
        return submissionId;
    }

    public int getContestId() {
        return contestId;
    }

    public String getProblemName() {
        return problemName;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public String getHandle() { return handle;}

    public String getVerdict() { return verdict;}

    public String getJsonString() { return jsonString;}

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String toString(){
        return problemName +"\n" + submissionId + "\n" + contestId + "\n" + descriptions + verdict + testCases + "Source: "+sourceCode;
    }
}