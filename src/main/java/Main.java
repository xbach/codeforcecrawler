import org.apache.commons.cli.*;

import java.util.HashSet;

public class Main {
    public static void main(String[] args) throws Exception {
        long startTime = System.nanoTime();
        // create Options object
        Options options = new Options();

        // add t option
        Option store = OptionBuilder.withArgName( "store" )
                .hasArg()
                .withDescription(  "Store data to specified folder" )
                .create( "store");

        Option lang = OptionBuilder.withArgName( "lang" )
                .hasArg()
                .withDescription(  "Language (e.g., Java 8)" )
                .create( "lang");

        Option testID = OptionBuilder.withArgName( "testID" )
                .hasArg()
                .withDescription(  "For testing purpose, crawl only project with specified ID (e.g., 893)" )
                .create( "testID");

        Option error = OptionBuilder.withArgName( "error" )
                .hasArg()
                .withDescription(  "Error kinds of interest" )
                .create( "error");

        Option numbug = OptionBuilder.withArgName( "numbug" )
                .hasArg()
                .withDescription(  "Number of buggy implementations considered for a correct submission" )
                .create( "numbug");

        Option problemIDs = OptionBuilder.withArgName( "problemIDs" )
                .hasArg()
                .withDescription(  "Read problem ids from a file" )
                .create( "problemIDs");

        Option connTimeout = OptionBuilder.withArgName( "connTimeout" )
                .hasArg()
                .withDescription(  "Connection time out" )
                .create( "connTimeout");

        Option readTimeout = OptionBuilder.withArgName( "readTimeout" )
                .hasArg()
                .withDescription(  "Read time out" )
                .create( "readTimeout");

        Option getProbOnly = OptionBuilder.withArgName( "getProbOnly" )
                .hasArg(false)
                .withDescription(  "Get problems only and write to file" )
                .create( "getProbOnly");

        Option probRange = OptionBuilder.withArgName( "probRange" )
                .hasArgs(2)
                .withDescription(  "Fetching problems in a specified range" )
                .create( "probRange");

        Option help = new Option( "help", "print this message" );

        options.addOption(store);
        options.addOption(lang);
        options.addOption(testID);
        options.addOption(error);
        options.addOption(numbug);
        options.addOption(problemIDs);
        options.addOption(getProbOnly);
        options.addOption(probRange);
        options.addOption(help);
        options.addOption(connTimeout);
        options.addOption(readTimeout);

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse( options, args);

        if(cmd.hasOption("help")){
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "CodeforceCrawler", options );
            return;
        }

        String storeDir = null;
        if(cmd.hasOption("store"))
            storeDir = cmd.getOptionValue("store");

        String language = null;
        if(cmd.hasOption("lang"))
            language = cmd.getOptionValue("lang");

        int testIDInt = -1;
        if(cmd.hasOption("testID")) {
            testIDInt = Integer.parseInt(cmd.getOptionValue("testID"));
        }

        int numberOfBugs = 1;
        if(cmd.hasOption("numbug"))
            numberOfBugs = Integer.parseInt(cmd.getOptionValue("numbug"));

        HashSet<String> errors = new HashSet<>();
        if(cmd.hasOption("error")) {
            String[] errorList = cmd.getOptionValue("error").split(",");
            for(String e: errorList){
                errors.add(e);
            }
        }

        String[] problems = null;
        if(cmd.hasOption("problemIDs")){
            String fileName = cmd.getOptionValue("problemIDs");
            problems = FileUtils.readLines(fileName);
        }

        Boolean getProblemOnly = false;
        if(cmd.hasOption("getProbOnly")) {
            getProblemOnly = true;
        }

        String[] problemRange = null;
        if(cmd.hasOption("probRange")){
            problemRange = cmd.getOptionValues("probRange");
            assert (problemRange != null);
            assert (problemRange.length == 2);
        }

        //System.out.println(problemRange[0]);
        //System.out.println(problemRange[1]);

        int rTimeout = 200000;
        int cTimeout = 10000;
        if(cmd.hasOption("readTimeout")){
            rTimeout = Integer.parseInt(cmd.getOptionValue("readTimeout"));
        }
        if(cmd.hasOption("connTimeout")){
            cTimeout = Integer.parseInt(cmd.getOptionValue("connTimeout"));
        }
        CodeforcesCrawler crawler = new CodeforcesCrawler(storeDir, language, testIDInt, errors, numberOfBugs, problems, getProblemOnly, problemRange, rTimeout, cTimeout);
        crawler.crawl();
        long endTime = System.nanoTime();

        // Execution time calculation.
        printHoursMinutesAndSeconds(endTime - startTime);
    }

    /**
     * Converts time given as nanoseconds to hh:mm:ss:ms format and prints it.
     *
     * @param time Time in nanoseconds.
     */
    private static void printHoursMinutesAndSeconds(long time) {
        time = time / 1000000L;
        long hours = time / 3600000;
        time = time % 3600000;
        long minutes = time / 60000;
        time = time % 60000;
        long seconds = time / 1000;
        time = time % 1000;
        System.out.println("\nCompleted fetching all successful submissions in "
                + hours + " hours, "
                + minutes + " minutes, "
                + seconds + " seconds, "
                + time + " milliseconds.");
    }
}
