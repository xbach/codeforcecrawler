import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;

import java.io.File;
import java.io.IOException;

public class TestCrawling {
    public static void main(String[] args) throws IOException {
        //test1();
        //test2();
        //test11();
        test12();
    }

    public static void test1(){
        //http://codeforces.com/contest/893/submission/32580923
        CodeforcesCrawler cr = new CodeforcesCrawler();
        cr.fetchSourceCodeAndTests("http://codeforces.com/contest/893/submission/32580923", new CodeforcesSubmission(32580923, 893, "893_A", "", "", "", ""));

    }

    public static void test11(){
        //http://codeforces.com/contest/893/submission/32605184
        CodeforcesCrawler cr = new CodeforcesCrawler();
        CodeforcesSubmission submission = new CodeforcesSubmission(32605184, 893, "893_A", "", "", "", "");
        cr.fetchSourceCodeAndTests("http://codeforces.com/contest/893/submission/32605184", submission);
        System.out.println(submission);
    }

    public static void test12(){
        //http://codeforces.com/contest/893/submission/32605331
        CodeforcesCrawler cr = new CodeforcesCrawler();
        CodeforcesSubmission submission = new CodeforcesSubmission(32605331, 893, "893_A", "", "", "", "");
        cr.fetchSourceCodeAndTests("http://codeforces.com/contest/893/submission/32605331", submission);
        System.out.println(submission);
    }

    public static void test2() throws IOException {
        String code = JDTUtils.readFile(new File("/home/xuanbach/workspace/codeforcecrawler/src/testprogram/CodeForces.java"));
        CompilationUnit cu = JDTUtils.parse(code);
        ASTRewrite rewriter = JDTUtils.rewriteMainClassName(cu, CodeforcesCrawler.MAIN_CLASS_NAME);
        Document document = new Document(code);

        TextEdit edits = rewriter.rewriteAST(document, null);
        try {
            edits.apply(document);
        } catch(MalformedTreeException e) {
            e.printStackTrace();
        } catch(BadLocationException e) {
            e.printStackTrace();
        }

        System.out.println(document.get());
    }
}
