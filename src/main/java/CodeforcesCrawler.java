import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.nio.file.Paths;

import com.google.common.util.concurrent.RateLimiter;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Author: Mayank Bhura (pc.mayank@gmail.com)
 * Crawler class that provides utils to crawl over Codeforces website, to download successful solutions of a user.
 */

public class CodeforcesCrawler {
    private static final double CODEFORCES_QPS_LIMIT = 5.0;
    private static final String CODEFORCES_SOLUTION_URL_FORMAT = "http://codeforces.com/contest/%s/submission/%s";
    private static final String CODEFORCES_SUBMISSIONS_URL_FORMAT = "http://codeforces.com/api/user.status?handle=%s&from=1";
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    private static final String VERDICT_OK = "OK";
    private static final String VERDICT_COMPILE_ERROR = "COMPILATION_ERROR";
    private static final String VERDICT_BUGGY = "BUG";

    private static final String PROBLEMSETS = "http://codeforces.com/api/problemset.problems";
    private static final String CONTEST = "http://codeforces.com/api/contest.status?contestId=%s";// e.g., 566

    private RateLimiter throttler = RateLimiter.create(CODEFORCES_QPS_LIMIT);
    private Map<String, String> alreadyFetchedProblemPage = new HashMap<>();

    private String programmingLanguage = "Java 8";
    private String codeforcesStoreDir = Paths.get(".").toAbsolutePath().normalize().toString() + FILE_SEPARATOR + "CodeforcesSubmissions";
    private String submissionFilePathFormat = getStoreDir() + FILE_SEPARATOR + "%s" + FILE_SEPARATOR + "%s";
    private int testID = -1;
    private HashSet<String> errors = new HashSet<>();
    private int numberOfBugs = 1;

    public static final String MAIN_CLASS_NAME = "Main";
    private List<Problem> problems = null;
    private Boolean getProbOnly = false;
    private String[] problemRange = null;
    private int readTimeout = 200000;
    private int connTimeout = 10000; //set timeout to 10 seconds

    public CodeforcesCrawler() {
        errors.add("WRONG_ANSWER");
    }

    private String getStoreDir() {
        return codeforcesStoreDir;
    }

    public CodeforcesCrawler(String store, String language, int testID, HashSet<String> errors, int numbug, String[] problems, Boolean getProbOnly, String[] problemRange, int rt, int ct) {
        if(store != null)
            codeforcesStoreDir = store;
        submissionFilePathFormat = getStoreDir() + FILE_SEPARATOR + "%s" + FILE_SEPARATOR + "%s";

        if(language != null)
            programmingLanguage = language;
        if(testID > -1)
            this.testID = testID;
        if(errors != null)
            for(String e: errors)
                this.errors.add(e);
        else
            this.errors.add("WRONG_ANSWER");
        numberOfBugs = numbug;
        if(problems != null){
            this.problems = new ArrayList<>();
            for(String p : problems){
                this.problems.add(new Problem(Integer.parseInt(p)));
            }
        }
        this.getProbOnly = getProbOnly;
        this.problemRange = problemRange;
        readTimeout = rt;
        connTimeout = ct;

    }

    public void crawl() throws IOException {
        if(this.problems == null) {
            System.out.println("Getting problems ...........");
            problems = getProblems();
            StringBuilder sb = new StringBuilder();
            for(Problem p : problems){
                sb.append(Integer.toString(p.getContestId()));
                sb.append("\n");
            }
            FileUtils.write2File("problems.txt", sb.toString());
            System.out.println("Done getting problems!");
            if(this.getProbOnly)
                return;
        } else {
            System.out.println("Finished reading problem ids from file, size = (" + problems.size() +")!");
        }
        System.out.println("Now iterating problems .......");

        Map<String, List<CodeforcesSubmission>> submissionByHandle = new HashMap<>();

        int minRange = 0;
        int maxRange = problems.size() - 1;
        if(this.problemRange != null){
            minRange = Integer.parseInt(this.problemRange[0]);
            maxRange = Integer.parseInt(this.problemRange[1]);
            assert (minRange >= 0);
            assert (maxRange < problems.size());
            assert (minRange <= maxRange);
        }

        while (minRange <= maxRange){
            Problem problem = problems.get(minRange);
            minRange += 1;

            if((problem.contestId == this.testID && this.testID > -1) || this.testID == -1) {
                System.out.println("Contest: "+problem.contestId);
                JSONObject response = parseRequest(String.format(CONTEST, problem.contestId));
                if(response == null) {
                    System.out.println("Cannot parse request! Continue to other contest ...");
                    continue;
                }
                JSONArray submissionJSON = (JSONArray) ((response.get("result")));
                ListIterator submissionIterator = submissionJSON.listIterator();
                while (submissionIterator.hasNext()) {
                    JSONObject submission = (JSONObject) submissionIterator.next();
                    String programmingLanguage = submission.get("programmingLanguage").toString();
                    if(programmingLanguage.compareTo(this.programmingLanguage) == 0){
                        int submissionId = Integer.parseInt(submission.get("id").toString());
                        String verdict = submission.get("verdict").toString();
                        if(this.errors.contains(verdict) || verdict.compareTo(VERDICT_OK) == 0) {
                            String problemName = Integer.toString(problem.contestId) + "_" + ((JSONObject) submission.get("problem")).get("index").toString();

                            String handle = submission.get("author").toString();

                            String uniqueKey = problemName + "_" + handle;

                            List<CodeforcesSubmission> submissionList = submissionByHandle.getOrDefault(uniqueKey, new ArrayList<>());

                            submissionList.add(new CodeforcesSubmission(submissionId, problem.contestId, problemName, programmingLanguage, handle, verdict, submission.toJSONString()));

                            submissionByHandle.put(uniqueKey, submissionList);
                        }
                    }
                }
                System.out.println("Created contest: "+problem.contestId);

            }
        }

        Set<Map.Entry<String, List<CodeforcesSubmission>>> setSubs = submissionByHandle.entrySet();
        Iterator<Map.Entry<String, List<CodeforcesSubmission>>> iterSub = setSubs.iterator();

        while(iterSub.hasNext()){
            Map.Entry<String, List<CodeforcesSubmission>> entry = iterSub.next();
            List<CodeforcesSubmission> submissions = entry.getValue();
            boolean errorOfInterest = false;
            for (CodeforcesSubmission s: submissions){
                if(errors.contains(s.getVerdict()))
                    errorOfInterest = true;
            }
            if(submissions.size() >= 2 && errorOfInterest){
                // We assume that the largest id is the OK one.
                Collections.sort(submissions, (o1, o2) -> o1.getSubmissionId() > o2.getSubmissionId() ? -1 : 1);
                if(submissions.get(0).getVerdict().compareTo(VERDICT_OK) == 0){
                    // Now crawl every version in submission list,
                    // the largest submission id is bug free one
                    int i = 0;
                    ListIterator<CodeforcesSubmission> iterateSubmission = submissions.listIterator();
                    while (iterateSubmission.hasNext() && i < this.numberOfBugs){
                        CodeforcesSubmission submission = iterateSubmission.next();
                        System.out.println("Fetching submission .....");
                        if(submission.getVerdict().compareTo(VERDICT_OK) == 0)
                            fetchSubmissionAndWriteToFile(submission, VERDICT_OK);
                        else {
                            fetchSubmissionAndWriteToFile(submission, VERDICT_BUGGY);
                            i++;
                        }
                    }
                }
            }
        }
    }

    private JSONObject parseRequest(String query) {
        String pageContent = null;
        try {
            pageContent = getPageContent(query);
        } catch (Exception e) {
            System.out.printf("Failed to fetch query: "+query+". Reason:" + e);
            return null;
        }

        JSONObject jsonResponse = null;
        try {
            jsonResponse = (JSONObject) new JSONParser().parse(pageContent);
        } catch (Exception e) {
            System.out.println("Failed to parse JSON response. Reason:" + e);
            return null;
        }
        return jsonResponse;
    }

    private List<Problem> getProblems() {
        Map<Integer, Problem> mapProbs = new HashMap<>();

        JSONObject jsonResponse = parseRequest(PROBLEMSETS);

        JSONArray problemJSON = (JSONArray) ((JSONObject)jsonResponse.get("result")).get("problems");
        ListIterator problemIterator = problemJSON.listIterator();
        while (problemIterator.hasNext()) {
            JSONObject problem = (JSONObject) problemIterator.next();
            //System.out.println(problem);
            int contestId = Integer.parseInt(problem.get("contestId").toString());
            //String index = problem.get("index").toString();
            Problem prob = mapProbs.getOrDefault(contestId, new Problem(contestId));
            //prob.addIndex(index);
            mapProbs.put(contestId, prob);

        }

        List<Problem> problems = new ArrayList<>();
        Iterator<Problem> iter = mapProbs.values().iterator();
        while (iter.hasNext()){
            Problem prob = iter.next();
            problems.add(prob);
        }

        return problems;
    }


    /**
     * Given a CodeforcesSubmission object for a user, downloads source code.
     * @param submission a submission.
     * @throws IOException
     */
    private void fetchSubmissionAndWriteToFile(CodeforcesSubmission submission, String verdict) throws IOException {
        System.out.printf("\n\n Fetching submission for problem: %s %s verdict = %s...\n", submission.getProblemName(), submission.getSubmissionId(), submission.getVerdict());
        String submissionUrl = String.format(CODEFORCES_SOLUTION_URL_FORMAT, submission.getContestId(), submission.getSubmissionId());

        try {
            fetchSourceCodeAndTests(submissionUrl, submission);
            System.out.printf("Successfully fetched source code for problem: %s %s\n", submission.getProblemName(), submission.getSubmissionId());
        } catch (Exception e) {
            System.out.printf("Could not fetch source code for problem: %s %s, as it is not public.\n", submission.getProblemName(), submission.getSubmissionId());
            return;
        }

        CompilationUnit cu = JDTUtils.parse(submission.getSourceCode());
        //String mainClassName = JDTUtils.extractMainClassName(cu);
        ASTRewrite rewriter = JDTUtils.rewriteMainClassName(cu, MAIN_CLASS_NAME);
        if(rewriter == null) {
            System.out.println("Not fetching code, continue ....");
            return;
        }
        String sourceCode = JDTUtils.writeAST(rewriter, submission.getSourceCode());

        // Write the source code to file.
        String fileName = /*mainClassName*/ MAIN_CLASS_NAME + getFileExtension(submission.getProgrammingLanguage());
        String folderPath = submission.getProblemName() + FILE_SEPARATOR + submission.getHandle().hashCode() + FILE_SEPARATOR + verdict + "_" + submission.getSubmissionId();
        String filePath = String.format(submissionFilePathFormat, folderPath, fileName);
        System.out.printf("Writing source file: %s\n", filePath);
        File file = new File(filePath);
        File parentDirectory = file.getParentFile();
        if (parentDirectory != null) {
            parentDirectory.mkdirs();
        }

        FileWriter fileWriter = new FileWriter(file);
        StringTokenizer tokenizer = new StringTokenizer(sourceCode, "\n");
        while (tokenizer.hasMoreTokens()) {
            fileWriter.write(tokenizer.nextToken() + "\n");
        }
        fileWriter.close();

        String statFilePath = String.format(submissionFilePathFormat, folderPath, "statistics");
        System.out.printf("Writing statistic file: %s\n", statFilePath);
        File statFile = new File(statFilePath);

        FileWriter statFileWriter = new FileWriter(statFile);
        statFileWriter.write(submission.getJsonString());
        statFileWriter.flush();
        statFileWriter.close();


        String descriptions = String.format(submissionFilePathFormat, folderPath, "descriptions");
        System.out.printf("Writing description file: %s\n", descriptions);
        File desFile = new File(descriptions);

        FileWriter desFileWriter = new FileWriter(desFile);
        desFileWriter.write(submission.getDescriptions());
        desFileWriter.flush();
        desFileWriter.close();

        // Write test files

        Iterator testIter = submission.getTestCases().iterator();
        String testFolder = folderPath + FILE_SEPARATOR + "tests" + FILE_SEPARATOR + "validated";

        int i = 0;
        while (testIter.hasNext()){
            CodeforcesTestCase test = (CodeforcesTestCase)testIter.next();
            String testName = "";
            if(test.getVerdict().contains(": OK")){
                testName = "p_"+i;// positive test
            }else testName = "n_"+i; // negative test

            // Write input
            String testInputFilePath = String.format(submissionFilePathFormat, testFolder , testName+"_"+"input");
            System.out.printf("Writing test input file: %s\n", testInputFilePath);
            File testInputFile = new File(testInputFilePath);
            if(testInputFile.getParentFile() != null)
                testInputFile.getParentFile().mkdirs();

            FileWriter testInputFileWriter = new FileWriter(testInputFile);
            testInputFileWriter.write(test.getInput());
            testInputFileWriter.flush();
            testInputFileWriter.close();

            // Write expected output
            String testOutputFilePath = String.format(submissionFilePathFormat, testFolder , testName+"_"+"output");
            System.out.printf("Writing test output file: %s\n", testOutputFilePath);
            File testOutputFile = new File(testOutputFilePath);

            FileWriter testOutputFileWriter = new FileWriter(testOutputFile);
            testOutputFileWriter.write(test.getExpectedOutput());
            testOutputFileWriter.flush();
            testOutputFileWriter.close();
            i++;
        }

        //Write example test files

        Iterator expTestIter = submission.getExampleTests().iterator();
        String expTestFolder = folderPath + FILE_SEPARATOR + "tests" + FILE_SEPARATOR + "examples";
        i = 0;
        while (expTestIter.hasNext()){
            CodeforcesTestCase test = (CodeforcesTestCase)expTestIter.next();
            String testName = i+"";

            // Write input
            String testInputFilePath = String.format(submissionFilePathFormat, expTestFolder , testName+"_"+"input");
            System.out.printf("Writing test input file: %s\n", testInputFilePath);
            File testInputFile = new File(testInputFilePath);
            if(testInputFile.getParentFile() != null)
                testInputFile.getParentFile().mkdirs();

            FileWriter testInputFileWriter = new FileWriter(testInputFile);
            testInputFileWriter.write(test.getInput());
            testInputFileWriter.flush();
            testInputFileWriter.close();

            // Write expected output
            String testOutputFilePath = String.format(submissionFilePathFormat, expTestFolder , testName+"_"+"output");
            System.out.printf("Writing test output file: %s\n", testOutputFilePath);
            File testOutputFile = new File(testOutputFilePath);

            FileWriter testOutputFileWriter = new FileWriter(testOutputFile);
            testOutputFileWriter.write(test.getExpectedOutput());
            testOutputFileWriter.flush();
            testOutputFileWriter.close();
            i++;
        }
    }


    /**
     * Given the submission URL, parses and returns the source code of the submission.
     * @param url URL of the submission.
     * @return Source code of the submission.
     */
    public void fetchSourceCodeAndTests(String url, CodeforcesSubmission submission) {
        String solutionPage = null;
        String problemPage = null;
        Document document = null;
        System.out.printf("Hitting URL: %s\n", url);
        try {
            // Parse the source code out of the HTML response.
            boolean stop = false;
            while (!stop) {
                document = Jsoup.parse(getPageContent(url));
                document.outputSettings(new Document.OutputSettings().prettyPrint(false));
                try {
                    String sourceCode = document.select("pre[class*=program-source]").get(0).text();
                    submission.setSourceCode(sourceCode);
                    stop = true;
                } catch (Exception e) {
                    System.out.println(e.getLocalizedMessage());
                    System.out.println("Request GET page content again for url: "+url);
                }
            }
        } catch (IOException | InterruptedException e) {
            System.out.printf("Failed to fetch html response for url: %s. Reason:%s \n", url, e);
        }


        Elements inputs = document.select("div[class*=file input-view]").select("pre");
        ListIterator inputIter = inputs.listIterator();
        List<String> inputValues = new ArrayList<>();
        while (inputIter.hasNext()){
            Element input = (Element) inputIter.next();
            inputValues.add(input.text());
        }

        Elements expectedOutputs = document.select("div[class*=file answer-view]").select("pre");
        ListIterator outputIter = expectedOutputs.listIterator();
        List<String> outputValues = new ArrayList<>();
        while (outputIter.hasNext()){
            Element output = (Element) outputIter.next();
            outputValues.add(output.text());
        }

        Elements verdicts = document.select("div[class*=infoline]");
        ListIterator verdictIter = verdicts.listIterator();
        List<String> verdictValues = new ArrayList<>();
        while (verdictIter.hasNext()){
            Element verdict = (Element) verdictIter.next();
            Elements verdictText = verdict.getElementsContainingOwnText("Verdict:");
            if(verdictText.size() > 0)
                verdictValues.add(verdictText.get(0).text());
        }

        for (int i = 0; i < inputValues.size(); i ++){
            String input = inputValues.get(i);
            String output = outputValues.get(i);
            String verdict = verdictValues.get(i);

            CodeforcesTestCase testCase = new CodeforcesTestCase(input, output, verdict);
            submission.addTestCase(testCase);
        }


        try {
            String problemUrl = "http://codeforces.com/contest/" + submission.getContestId() + "/problem/" + submission.getProblemName().split("_")[1];
            String fetched = alreadyFetchedProblemPage.getOrDefault(submission.getProblemName(), null);
            if(fetched == null) {
                problemPage = getPageContent(problemUrl);
                alreadyFetchedProblemPage.put(submission.getProblemName(), problemPage);
            } else problemPage = fetched;

        } catch (IOException | InterruptedException e) {
            System.out.printf("Failed to fetch html response for url: %s. Reason:%s \n", url, e);
        }

        // Parse problem page
        // Parse the source code out of the HTML response.
        Document probDocument = Jsoup.parse(problemPage);
        probDocument.outputSettings(new Document.OutputSettings().prettyPrint(false));
        Element examples = probDocument.select("div[class*=sample-test]").get(0);
        Elements exampleInputs = examples.select("div[class*=input]").select("pre");
        Elements exampleOutputs = examples.select("div[class*=output]").select("pre");
        Element descriptions = probDocument.select("div[class*=problem-statement]").get(0);
        for (int i = 0; i < exampleInputs.size(); i++) {
            String input = exampleInputs.get(i).text();
            String output = exampleOutputs.get(i).text();
            CodeforcesTestCase testCase = new CodeforcesTestCase(input, output, null);
            submission.addExampleTestCase(testCase);
        }

        submission.setDescriptions(descriptions.text());

        return;
    }



    /**
     * Given a URL, fetches the html page contents of it.
     * @param urlToGet URL of webpage whose HTML is to be fetched.
     * @return HTML response of GET request to urlToGet.
     * @throws IOException
     * @throws InterruptedException
     */
    private String getPageContent(String urlToGet) throws IOException, InterruptedException {
        URL url = new URL(urlToGet);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(connTimeout);
        connection.setReadTimeout(readTimeout);
        connection.setRequestMethod("GET");
        String USER_AGENT = "Mozilla/5.0";
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setUseCaches(false);
        int responseCode = 0, count = 0;
        while (responseCode != 200 && count < 5) {
            try {
                throttler.acquire();
                responseCode = connection.getResponseCode();
                assert (responseCode == 200);
            } catch (java.net.SocketTimeoutException e) {
                System.out.printf("Failed to connect to url: %s. Reason: %s\n", urlToGet, e);
            }
            catch (IOException e) {
                System.out.printf("Failed to connect to url: %s. Reason: %s\n", urlToGet, e);
            }
            count++;
        }

        if(count >= 5) {
            System.out.println("Timed out after 5 trials!");
            return null;
        }
        System.out.println("Now get page content .....");

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine, response = new String();
        while ((inputLine = bufferedReader.readLine()) != null) {
            response = response + inputLine + "\n";
        }
        bufferedReader.close();

        //System.out.println(response);

        if(response.isEmpty()) {
            System.out.println("Empty response!!!");
            return null;
        }

        return response;
    }

    /**
     * Given programming language, returns the appropriate program file extension for it.
     * @param programmingLanguage The programming language.
     * @return String representing the file extension. Example: ".c", ".cpp", etc.
     */
    private String getFileExtension(String programmingLanguage) {
        if (programmingLanguage.contains("Java")) {
            return ".java";
        } else if (programmingLanguage.contains("Py")) {
            return ".py";
        } else if (programmingLanguage.contains("GNU C++")) {
            return ".cpp";
        } else if (programmingLanguage.contains("GNU C")) {
            return ".c";
        } else {
            return "";
        }
    }

    private class Problem {
        private final int contestId;
        //private final ArrayList<String> indexes;

        public Problem(int contestId){
            this.contestId = contestId;
            //this.indexes = new ArrayList<>();
        }

        public int getContestId() {
            return contestId;
        }

        //public ArrayList<String> getIndex() {
        //    return indexes;
        //}

        //public void addIndex(String index) { indexes.add(index); }
    }

}