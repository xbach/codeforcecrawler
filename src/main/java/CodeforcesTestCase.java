/**
 * Public class to wrap submission details:
 * 1. submission Id
 * 2. contest Id
 * 3. problem name
 * 4. programming language name
 */
public class CodeforcesTestCase {
    private final String input;
    private final String expectedOutput;
    private final String verdict;

    public CodeforcesTestCase(String input, String expectedOutput, String verdict){
        this.input = input;
        this.expectedOutput = expectedOutput;
        this.verdict = verdict;
    }

    public String getInput() {
        return input;
    }

    public String getExpectedOutput() {
        return expectedOutput;
    }

    public String getVerdict() {
        return verdict;
    }

    @Override
    public String toString(){
        return "Input: " + input + "\n" + "Output: " + expectedOutput;
    }

}