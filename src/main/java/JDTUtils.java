import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import org.eclipse.jdt.core.dom.*;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.text.edits.MalformedTreeException;
import org.eclipse.text.edits.TextEdit;


public class JDTUtils {
    //use ASTParse to parse string
    public static CompilationUnit parse(String str) {
        ASTParser parser = ASTParser.newParser(AST.JLS3);
        parser.setSource(str.toCharArray());
        parser.setKind(ASTParser.K_COMPILATION_UNIT);

        final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

        return cu;
    }

    public static String extractMainClassName(CompilationUnit cu) {
        ListIterator<AbstractTypeDeclaration> types = cu.types().listIterator();
        while (types.hasNext()){
            AbstractTypeDeclaration typeDec = types.next();
            if(typeDec instanceof TypeDeclaration){
                MethodDeclaration[] mdecs = ((TypeDeclaration) typeDec).getMethods();
                for(int i = 0; i< mdecs.length; i++){
                    if(mdecs[i].getName().toString().compareTo("main") == 0
                            && Modifier.isPublic(mdecs[i].getModifiers())
                            && Modifier.isStatic(mdecs[i].getModifiers())) {
                        return typeDec.getName().getIdentifier();
                    }
                }
            }
        }

        return null;
    }

    public static ASTRewrite rewriteMainClassName(CompilationUnit cu, String mainClass) {
        if(cu == null)
            return null;

        List<SimpleName> names = new ArrayList<>();
        String className = extractMainClassName(cu);
        if(className == null)
            return null;

        AST ast = cu.getAST();
        ASTRewrite rewriter = ASTRewrite.create(ast);
        cu.accept(new ASTVisitor() {
            @Override
            public boolean visit(SimpleName node) {
                if(node.getIdentifier().compareTo(className) == 0)
                names.add(node);
                return super.visit(node);
            }
        });

        for(int i = 0; i < names.size(); i++){
            SimpleName toRep = names.get(i);
            SimpleName nameMain = ast.newSimpleName(mainClass);
            rewriter.replace(toRep, nameMain, null);
        }

        return rewriter;
    }

    public static String writeAST(ASTRewrite rewriter, String sourceCode) {
        Document document = new Document(sourceCode);

        TextEdit edits = rewriter.rewriteAST(document, null);
        try {
            edits.apply(document);
        } catch(MalformedTreeException e) {
            e.printStackTrace();
        } catch(BadLocationException e) {
            e.printStackTrace();
        }

        return document.get();
    }

    public static String readFile(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        try {
            StringBuilder content = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line);
                content.append('\n');
            }
            return content.toString();
        }
        finally {
            if (reader != null) reader.close();
        }
    }
}
