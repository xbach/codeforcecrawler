

import java.io.*;
import java.util.*;

public class CodeForces {
    public static void main (String[] args) throws IOException {
        Scanner br=new Scanner(System.in);
        /*String input = "3\n" +
                "1\n" +
                "1\n" +
                "2";

        Scanner br=new Scanner(input);*/

        CodeForces cf = new CodeForces();
        int n=br.nextInt();
        int temp=3;
        for(int i=0;i<n;i++){
            int res=br.nextInt();
            if(res==temp){
                System.out.println("NO");
                System.exit(0);
            }
            if(res==1)
                temp=temp==2?3:2;
            else if(res==2)
                temp=temp==1?3:1;
        }
        System.out.println("YES");
    }

}