
// Put this file under: ~/workspace/jpf/jpf-core/src/classes/java/nio/util
// and then compile jpf again
// command to run: java -jar ~/workspace/jpf/jpf-core/build/RunJPF.jar Example.jpf
// compile: javac -cp .:/home/xuanbach/workspace/codeforcecrawler/lib/junit-4.12.jar:/home/xuanbach/workspace/codeforcecrawler/out/production/codeforcecrawler/ -g ExampleTests.java

import java.io.*;

public class Scanner {
    String[] arr;
    int count = 0;

    public static void main(String[] args) throws IOException {
        String input = "3\n" +
                "1\n" +
                "1\n" +
                "2";
        StringBufferInputStream s = new StringBufferInputStream(input);
        System.setIn(s);
        Scanner sc = new Scanner(System.in);
        System.out.println(sc.nextInt());
    }

    public Scanner(InputStream inputStream) throws IOException {
        String textBuilder = "";
        /*try (Reader reader = new BufferedReader(new InputStreamReader
                (inputStream, "UTF-8"))) {//Charset.forName(StandardCharsets.UTF_8.name())
            int c;
            while ((c = reader.read()) != -1) {
                textBuilder += ((char) c);
            }
        }*/

        InputStreamReader reader = new InputStreamReader
                (inputStream, "UTF-8");
        int c;
        while ((c = reader.read()) != -1) {
            textBuilder += ((char) c);
        }

        arr = textBuilder.split("\n");

    }

    public Scanner(String inputStream) throws IOException {
        arr = inputStream.split("\n");

    }

    public int nextInt(){
        String a = arr[count++];
        return Integer.parseInt(a);
    }
}
