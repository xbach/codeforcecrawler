

import org.junit.Test;


import java.io.StringBufferInputStream;

import static org.junit.Assert.assertTrue;

public class ExampleTests {

    public static void main (String[] args) throws Exception {
        ExampleTests t = new ExampleTests();
        t.test1();
    }

    @Test
    // test method to add two values
    public void test1() throws Exception {
        String input = "3\n" +
                "1\n" +
                "1\n" +
                "2";
        StringBufferInputStream s = new StringBufferInputStream(input);
        System.setIn(s);
        ConsoleOutputCapturer c = new ConsoleOutputCapturer();
        c.start();
        CodeForces.main(null);
        String out = c.stop();
        System.out.println("Output = "+out);
        assertTrue(out.trim().equals("NO".trim()));
    }

    /*@Test
    // test method to add two values
    public void test1() throws Exception {
        String input = "3\n" +
                "1\n" +
                "1\n" +
                "2";

        ConsoleOutputCapturer c = new ConsoleOutputCapturer();
        c.start();
        CodeForces.main(null);
        String out = c.stop();
        System.out.println("Output = "+out);
        assertTrue(out.trim().equals("YES".trim()));
    }*/
}