# CodeforcesCrawler
Web crawler that downloads all unique, successul, public solutions of a user on Codeforces.

Feedback link: https://goo.gl/forms/kijsxr0BatZhbTxE3

Usage Instructions:
---------------------

1. Download CodeforcesCrawler.jar from the repository.

2. $ java -jar CodeforcesCrawler.jar \<username\>

3. List of errors to choose to collect: WRONG_ANSWER, TIME_OUT, etc.

Example:
---------
mvn clean compile assembly:single

java -jar target/codeforcescrawler-jar-with-dependencies.jar -help

java -jar target/codeforcescrawler-jar-with-dependencies.jar -store . -lang "Java 8" -testID 893

java -jar target/codeforcescrawler-jar-with-dependencies.jar -store . -lang "Java 8" -error WRONG_ANSWER -probRange 0 192 -problemIDs problems.txt

Features:
----------
1. Crawl solutions

2. Codeforces API asks us to follow a max of 5QPS for GET requests, so I have added a throttler that throttles queries above 5QPS. Used the Guava API for that.
